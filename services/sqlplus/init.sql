SET ECHO ON;

CREATE PLUGGABLE DATABASE APPDB
  ADMIN USER root IDENTIFIED BY password
  ROLES = (dba)
  DEFAULT TABLESPACE APPDB
    DATAFILE '/u02/app/oracle/oradata/ORCL/APPDB/demo01.dbf' SIZE 250M AUTOEXTEND ON
  FILE_NAME_CONVERT = ('/u02/app/oracle/oradata/ORCL/pdbseed/',
                       '/u02/app/oracle/oradata/ORCL/APPDB/')
  STORAGE (MAXSIZE 5G)
  PATH_PREFIX = '/u02/app/oracle/oradata/ORCL/APPDB/';

  alter pluggable database APPDB open;
  alter pluggable database APPDB save state;

  ALTER SESSION SET CONTAINER = APPDB;

  --alter session set "_ORACLE_SCRIPT"=true;
  CREATE USER APPDB IDENTIFIED BY appdb;
  GRANT CONNECT, CREATE SESSION, RESOURCE, DBA TO appdb;
  GRANT UNLIMITED TABLESPACE TO appdb;

  exit;
