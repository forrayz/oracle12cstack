#!/usr/bin/env bash
set -o errexit
set -o pipefail
set -o nounset
echo  "╔════════════════════════════════════════════════════════════╗"
echo  "║    deploy.sh BASH scipt                                    ║"
echo  "║     Author        : Zoltán Forray                          ║"
echo  "║     deploy job                                             ║"
echo  "║                                                            ║"
echo  "╚════════════════════════════════════════════════════════════╝"
trap "set +x; sleep 1; set -x" DEBUG
CLIENT=customer1
ENV=feature
INFRA=docker
export ENVIRONMENT_PREFIX=${CLIENT}-${ENV}-${INFRA}
pwd
docker-compose -p ${ENVIRONMENT_PREFIX} --file services/docker-compose.yaml up --build --force-recreate --renew-anon-volumes --detach
docker build -t sqlplus services/sqlplus
bash scripts/wait-for-healthy-container.sh ${ENVIRONMENT_PREFIX}_oracle12c_1 120
export ORACLE_PORT=$( docker inspect --format '{{ (index (index .NetworkSettings.Ports "1521/tcp") 0).HostPort }}' ${ENVIRONMENT_PREFIX}_oracle12c_1)
docker-compose -p ${ENVIRONMENT_PREFIX}  logsdocker run --rm -t sqlplus sqlplus "sys/Oradoc_db1@(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(Host=localhost)(Port=${ORACLE_PORT}))(CONNECT_DATA=(SID=ORCLCDB))) as sysdba" @/init.sq